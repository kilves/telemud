from telebot.types import ReplyKeyboardMarkup
import threading
import config
from lang import translations
from enemy import Enemy

rooms = {}


class Room(object):
    def __init__(self, room):
        self.tag = room["tag"]
        self.lang = room["lang"]
        if "resources" in room:
            self.resources = room["resources"]
        else:
            self.resources = {}
        self.max_enemies = room["max_enemies"]
        self.enemy_spawners = room["enemy_spawners"]
        self.buttons = room["buttons"]
        self.enemies = []
        self.timer = 0

    def refresh(self, room):
        self.tag = room["tag"]
        self.lang = room["lang"]
        self.resources = room["resources"]
        self.buttons = room["buttons"]
        self.max_enemies = room["max_enemies"]
        self.enemy_spawners = room["enemy_spawners"]

    def build_reply_keyboard(self, user):
        """
        Build keyboard for user and room
        :param user the user
        """
        markup = ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
        room_lang = self.lang[user.lang]

        if len(self.resources) > 0:
            for resource in self.resources:
                action_text = translations[user.lang]["gather"]
                item_text = translations[user.lang][resource["tag"]]
                markup.row("*{0} {1}*".format(action_text, item_text))

        if len(self.enemies) > 0:
            unique_enemies = []
            for enemy in self.enemies:
                enemy_name = enemy.lang[user.lang]["name"]
                if enemy_name not in unique_enemies:
                    unique_enemies.append(enemy_name)
            for unique_enemy in unique_enemies:
                markup.row("*Attack {0}*".format(unique_enemy))

        for key in self.buttons:
            markup.row(room_lang[key])

        markup.row(translations[user.lang]["look_around"])

        return markup

    def spawn_enemy(self, tag):
        enemy = Enemy.create(tag)
        if enemy:
            self.enemies.append(enemy)

    def update(self):
        """Update room, spawn stuff etc"""
        self.timer += 1
        for spawner in self.enemy_spawners:
            if self.timer % int(spawner["frequency"]) == 0 and len(self.enemies) < self.max_enemies:
                self.spawn_enemy(spawner["enemy"])


    def save(self):
        """Saves the room to database"""
        config.db.rooms.update_one({"tag": self.tag}, {"$set": {
            "resources": self.resources
        }})

    def remove_resource(self, resource, amount):
        """
        Removes a given amount of resources from the room
        :param resource the resource tag to remove
        :param amount the amount to remove
        """
        for res in self.resources:
            if res["tag"] == resource:
                res["amount"] -= amount
                if res["amount"] < 0:
                    res["amount"] = 0
        self.save()

    def render(self, user):
        """
        Render the room
        :param user the user to render the room for
        """
        description_txt = self.lang[user.lang]["description"]
        meta_txt = self.lang[user.lang]["meta"]
        keyboard_markup = self.build_reply_keyboard(user)
        resource_txt = ""
        enemies_txt = ""

        if len(self.resources) > 0:
            resource_txt = "*resources:*\n"
            for resource in self.resources:
                resource_amount = resource["amount"]
                resource_name = translations[user.lang][resource["tag"]]
                resource_txt += "{0}: {1} kg\n".format(resource_name, resource_amount)
            resource_txt += "\n"
        print(len(self.enemies))
        if len(self.enemies) > 0:
            enemies_txt = "*\nenemies:*\n"
            for enemy in self.enemies:
                enemies_txt += "{0}\n".format(enemy.lang[user.lang]["name"])

        config.bot.send_message(user.telegram_id,
                                text="{0}\n{1}{2}\n_{3}_".format(description_txt, resource_txt, enemies_txt, meta_txt),
                                parse_mode="Markdown",
                                reply_markup=keyboard_markup)

def update():
    threading.Timer(1, update).start()
    for room in rooms:
        rooms[room].update()

def get(tag):
    if tag in rooms:
        return rooms[tag]


def load_all():
    """Load all rooms from database"""
    all_rooms = config.db.rooms.find()
    room_count = 0
    for room in all_rooms:
        room_count += 1
        if room["tag"] not in rooms:
            rooms[room["tag"]] = Room(room)
        else:
            rooms[room["tag"]].refresh(room)

    print("Loaded {} rooms from the database".format(room_count))
