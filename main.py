import config
import room
import user
import recipe
from lang import translations


@config.bot.message_handler(commands=["start"])
def start_adventure(message):
    if message.from_user.username != "Kilves":
        config.bot.reply_to(message, "Access restricted")
        return
    if message.chat.type != "private":
        config.bot.reply_to(message, "This bot only works in private messages")
        return

    if config.db.users.find_one({"telegram_id": message.from_user.id}):
        config.bot.reply_to(message, "The adventure has already started.")
        return

    new_user = {
        "username": message.from_user.username,
        "first_name": message.from_user.first_name,
        "last_name": message.from_user.last_name,
        "telegram_id": message.from_user.id,

        "room": "start",
        "lang": "en_US",

        "items": [
            [None, None, None],
            [None, None, None],
            [None, None, None],
            [None, None, None],
        ],
        "hp": 100,
        "max_hp": 100
    }
    config.db.users.insert_one(new_user)
    user.load_all()
    room.get("start").render(user.get(message.from_user.id))


def reverse_translate(phrase, language):
    for key, translation in translations[language].items():
        if translation == phrase:
            return key


def parse_side_action(this_user, message):
    command_txt = message.text.split(" ")[0]
    side_txt = " ".join(message.text.split(" ")[1:])
    command = reverse_translate(command_txt, this_user.lang)
    side = reverse_translate(side_txt, this_user.lang)

    if command == "gather":
        this_user.gather(side, this_user.room)

    if command == "attack":
        this_user.start_combat(side_txt)


@config.bot.message_handler(func=lambda message: message.text[0] != "/")
def parse_command(message):
    if message.chat.type != "private":
        config.bot.reply_to(message, "This bot only works in private messages")
        return

    this_user = user.get(message.from_user.id)

    if not this_user:
        return

    side_action = message.text.split("*")
    if len(side_action) == 3:
        if side_action[0] == "" and side_action[2] == "":
            message.text = side_action[1]
            parse_side_action(this_user, message)
            return

    button_tag = ""

    current_room = room.get(this_user.room)

    if message.text == translations[this_user.lang]["look_around"]:
        current_room.render(this_user)
        return

    room_locale = current_room.lang[this_user.lang]

    button_tags = {value: key for key, value in room_locale.items() if key in current_room.buttons.keys()}
    if message.text in button_tags:
        button_tag = button_tags[message.text]

    if "duration" not in current_room.buttons[button_tag]:
        this_user.move_to(current_room.buttons[button_tag]["target"])
    else:
        this_user.start_moving(current_room.buttons[button_tag]["target"], current_room.buttons[button_tag]["duration"])

@config.bot.callback_query_handler(func=lambda callback: True)
def callback_query(callback):
    #TODO multi attack
    if callback.data == "ATTACK":
        u = user.get(callback.from_user.id)
        u.combat_display_id = callback.message.message_id
        u.perform_attack()



@config.bot.message_handler(commands=["items"])
def get_items(message):
    this_user = user.get(message.from_user.id)
    this_user.show_inventory()

def main():
    user.load_all()
    room.load_all()
    recipe.load_all()
    user.update()
    room.update()
    config.bot.polling()

if __name__ == "__main__":
    main()