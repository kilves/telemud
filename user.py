import threading
from random import randint

from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
import config
import room
import recipe
import item
from lang import translations
users = {}


class User(object):

    def __init__(self, user):
        """Initializer builds from db object"""
        self.username = user["username"]
        self.first_name = user["first_name"]
        self.last_name = user["last_name"]
        self.telegram_id = user["telegram_id"]
        self.room = user["room"]
        self.lang = user["lang"]
        self.items = []
        for i in range(0, 4):
            self.items.append([])
            for u in range(0, 3):
                self.items[i].append(
                    item.get(user["items"][i][u])
                )

        self.hp = user["hp"]
        self.max_hp = user["max_hp"]

        self.move_target = ""
        self.move_duration = -1

        self.gather_duration = -1
        self.gather_gives = None
        self.gathering_from = ""

        self.combat_enemy = None
        self.last_combat_message = ""
        self.combat_display_id = -1
        self.combat_turn = True

    def refresh(self, user):
        self.username = user["username"]
        self.first_name = user["first_name"]
        self.last_name = user["last_name"]
        self.telegram_id = user["telegram_id"]
        self.room = user["room"]
        self.lang = user["lang"]
        self.hp = user["hp"]
        self.max_hp = user["max_hp"]

    def start_combat(self, enemy):
        """
        Start a combat with an enemy in the current room
        :param enemy the enemy, in translated text
        """
        this_room = room.get(self.room)
        if enemy not in [enemy.lang[self.lang]["name"] for enemy in this_room.enemies]:
            returnrender_combat_key
        if self.combat_enemy:
            return

        for an_enemy in this_room.enemies:
            if an_enemy.lang[self.lang]["name"] == enemy:
                an_enemy.combat_user = self
                self.combat_enemy = an_enemy
        self.combat_display_id = config.bot.send_message(self.telegram_id,
                                                         text=self.render_combat_information(),
                                                         reply_markup=self.render_combat_keyboard())

    def end_combat(self):
        this_room = room.get(self.room)
        if self.combat_enemy not in this_room.enemies:
            return
        if self.combat_enemy.dead:
            this_room.enemies.remove(self.combat_enemy)
            self.last_combat_message = ""
            self.combat_turn = True


    def refresh_combat_display(self):
        config.bot.edit_message_text(chat_id=self.telegram_id,
                                     message_id=self.combat_display_id,
                                     text=self.render_combat_information(),
                                     reply_markup=self.render_combat_keyboard())

    def perform_attack(self):
        """
        Perform the attack
        :param message_id the message id the attack came from, needed to edit it
        """
        #TODO implement multi-attack system here
        if not self.combat_enemy or not self.combat_turn:
            return

        min_value = 5
        max_value = 10
        self.combat_turn = False

        dealt_damage = self.combat_enemy.deal_damage([min_value, max_value])
        self.last_combat_message = "{0} dmg to {1}".format(dealt_damage, self.combat_enemy.lang[self.lang]["name"])
        if self.combat_enemy.dead:
            self.combat_enemy.hp = 0
            self.last_combat_message += "\nEnemy died!"

        self.refresh_combat_display()

        #clear the data
        if self.combat_enemy.dead:
            self.last_combat_message = ""
            self.combat_enemy = None
            self.combat_display_id = -1


    def render_combat_keyboard(self):
        #TODO implement multi-attack system here
        markup = InlineKeyboardMarkup(row_width=1)
        markup.row(InlineKeyboardButton(text="Attack", callback_data="ATTACK"))
        return markup

    def render_combat_information(self):
        if not self.combat_enemy:
            return ""
        enemy = self.combat_enemy
        combat_screen =  "You are fighting {0}!\n\n{0}: {1}/{2}hp\nYou: {3}/{4}hp\n\n{5}"
        enemy_name = self.combat_enemy.lang[self.lang]["name"]
        return combat_screen.format(enemy_name, enemy.hp, enemy.max_hp, self.hp, self.max_hp, self.last_combat_message)

    def render_crafting_information(self):
        crafting_info = "Craft one of the following items:"
        return crafting_info

    def can_craft(self, target_recipe):
        if target_recipe.available_in != self.room or target_recipe.available_in == "":
            return False
        return self.has_required_ingredients(target_recipe)

    def has_required_ingredients(self, target_recipe):
        required_amount = len(target_recipe.ingredients)
        has_amount = 0
        for user_item in self.items:
            for ingredient_tag in target_recipe.ingredients:
                ingredient_required = target_recipe.ingredients[ingredient_tag]
                if ingredient_tag == user_item["tag"] and ingredient_required > user_item["amount"]:
                    has_amount += 1
            if has_amount >= required_amount:
                return True
        return False


    def render_crafting_keyboard(self):
        recipes = recipe.recipes
        keyboard = InlineKeyboardMarkup(row_width=2)
        for recipe_tag in recipes:
            if not self.can_craft(recipes[recipe_tag]):
                continue
            keyboard.row(InlineKeyboardButton(text=recipes[recipe_tag]["lang"][self.lang]["name"],
                                              callback_data="CRAFT {0}".format(recipe_tag)))

    def deal_damage(self, damage):
        damage = randint(damage[0], damage[1])
        self.hp -= damage
        self.last_combat_message = "{0} dmg to you!".format(damage)
        self.refresh_combat_display()

    def give_turn(self):
        self.combat_turn = True
        if self.combat_enemy.dead:
            self.end_combat()


    def save(self):
        """Saves the user to database"""
        config.db.users.update_one({"telegram_id": self.telegram_id}, {"$set": {
            "username": self.username,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "room": self.room,
            "lang": self.lang,
            "items": [[self.items[y][x]._id for x in range(0, 3)] for y in range(0, 4)]
        }})

    def show_inventory(self):
        """Show the user inventory"""
        this_lang = translations[self.lang]
        inventory_text = "Inventory:\n\n"
        inventory_keyboard = InlineKeyboardMarkup(row_width=3)
        y = 0
        for item_row in self.items:
            row_buttons = []
            x = 0
            for one_item in item_row:
                if one_item is None:
                    item_text = "No item"
                else:
                    item_text = one_item.tag
                row_buttons.append(InlineKeyboardButton(text=item_text, callback_data="I {0}:{1}".format(x, y)))
                x += 1
            print(*row_buttons)

            y += 1
            inventory_keyboard.row(*row_buttons)
        config.bot.send_message(self.telegram_id, text=inventory_text, reply_markup=inventory_keyboard)

    def gather(self, resource, from_room):
        """
        Starts gathering resource from room
        :param resource the resource template tag
        :param from_room the room tag to collect from
        """
        target_room = room.get(from_room)
        if target_room:
            for available_resource in target_room.resources:
                if available_resource["tag"] == resource and available_resource["amount"] > 0:
                    self.gather_gives = item.from_template(resource)
                    self.gather_gives.amount = min(available_resource["gives"], available_resource["amount"])
                    self.gather_duration = available_resource["duration"]
                    self.gathering_from = from_room
                    config.bot.send_message(self.telegram_id, text=translations[self.lang]["action_progress"])
                    break
                elif available_resource["tag"] == resource and available_resource["amount"] == 0:
                    config.bot.send_message(self.telegram_id,
                                            text="{0} {1}".format(translations[self.lang][resource], translations[self.lang]["not_left"]))
        else:
            config.bot.send_message(self.telegram_id, text="ROOM DOES NOT EXIST")

    def give_item(self, item):
        this_lang = translations[self.lang]
        got_text = "{0} {1} {2}".format(this_lang["got_item"], item.amount, this_lang[item.tag])
        # First check for stacks
        for item_row in self.items:
            for inventory_item in self.items:
                if inventory_item and inventory_item.tag == item.tag and inventory_item.amount + item.amount <= inventory_item.max_amount:
                    inventory_item.amount += item.amount
                    config.bot.send_message(self.telegram_id, text=got_text)
                    return
                elif inventory_item and inventory_item.tag == item.tag and inventory_item.amount + item.amount > inventory_item.max_amount:
                    amount_to_add = inventory_item.amount + item.amount - inventory_item.max_amount
                    inventory_item.amount += amount_to_add
                    item.amount -= amount_to_add


        # No stacks or they are full, try to make a new stack
        for item_row in self.stacks:
            for inventory_item in self.items:
                if not inventory_item:
                    inventory_item = item.Item({
                        "tag": item.tag,
                        "type": item.type,
                        "max_amount": item.max_amount
                    })
                    inventory_item.amount = item.amount
                    inventory_item.save()



        for i in self.items:
            if i["tag"] == item["item"]:
                i["amount"] += item["amount"]
                config.bot.send_message(self.telegram_id, text="{0} {1}kg {2}!".format(this_lang["got_item"], item["amount"], this_lang[item["item"]]))
                self.save()
                return

        self.items.append({
            "tag": item["item"]

        })
        config.bot.send_message(self.telegram_id, text="{0} {1}kg {2}!".format(this_lang["got_item"], item["amount"], this_lang[item["item"]]))
        self.save()


    def update(self):
        """Updates user counters, gives items, etc"""

        if self.move_duration > 0:
            self.move_duration -= 1
        if self.gather_duration > 0:
            self.gather_duration -= 1

        if self.gather_duration == 0:
            room_to_remove = room.get(self.gathering_from)
            self.gathering_from = ""
            self.gather_duration = -1
            self.give_item(self.gather_gives)
            room_to_remove.remove_resource(self.gather_gives.tag, self.gather_gives.amount)
            self.gather_gives = None
            self.gather_duration = -1

        if self.move_duration == 0:
            self.move_to(self.move_target)
            self.move_target = ""
            self.move_duration = -1

    def start_moving(self, target, duration):
        """Starts moving to target room with a given duration"""
        target_room = room.get(target)
        if target_room:
            self.move_duration = duration
            self.move_target = target
            config.bot.send_message(self.telegram_id, text=translations[self.lang]["action_progress"])
        else:
            config.bot.send_message(self.telegram_id, text="ROOM DOES NOT EXIST")

    def move_to(self, target):
        """
        Moves to a room
        :param target the room tag to move to
        """
        target_room = room.get(target)
        if target_room:
            self.room = target
            target_room.render(self)
        else:
            config.bot.send_message(self.telegram_id, text="CANNOT MOVE TO ROOM {0}".format(target))

        self.save()


def update_namedata(update_objects):
    """
    Updates name data from telegram update object
    :param update_object the update object
    """
    for update_object in update_objects:
        for user in users:
            users[user].username = update_object.from_user.username
            users[user].first_name = update_object.from_user.first_name
            users[user].last_name = update_object.from_user.last_name


def update():
    threading.Timer(1, update).start()
    for user in users:
        users[user].update()


def get(tag):
    if tag in users:
        return users[tag]


def load_all():
    """Load all users from db"""
    u = config.db.users.find()
    u_count = 0
    for user in u:
        u_count += 1
        if user["telegram_id"] not in users:
            users[user["telegram_id"]] = User(user)
        else:
            users[user["telegram_id"]].refresh(user)
    print("Loaded {0} users from the database".format(u_count))