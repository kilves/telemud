import config

recipes = {}

class Recipe(object):
    def __init__(self, recipe):
        self.tag = recipe["tag"]
        self.ingredients = recipe["ingredients"]
        self.result = recipe["result"]
        self.available_in = recipe["available_in"]
        self.lang = recipe["lang"]

def load_all():
    for recipe in config.db.recipes.find():
        recipes[recipe["tag"]] = Recipe(recipe)
