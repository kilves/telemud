import config


class Item(object):
    def __init__(self, template=None):
        if template is None:
            self._id = None
            self.tag = ""
            self.amount = 0
            self.type = ""
            self.max_amount = 64
        else:
            self._id = None
            self.tag = template["tag"]
            self.amount = 1
            self.type = template["type"]
            self.max_amount = template["max_amount"]

    def save(self):
        """
        Saves the item to database
        """
        if self._id:
            config.db.item_templates.update_one({"_id": self._id}, {"$set": {
                "tag": self.tag,
                "amount": self.amount,
                "type": self.type,
                "max_amount": self.max_amount,
            }})
        else:
            self._id = config.db.items.insert_one({
                "tag": self.tag,
                "amount": self.amount,
                "type": self.type,
                "max_amount": self.max_amount
            }).inserted_id

def get(tag):
    """
    Get item from db
    :param tag: the tag
    :return: the item
    """
    item = config.db.items.find_one({"tag": tag})
    if item:
        return Item(item)

def from_template(tag):
    """
    Generates item from template
    :param tag: the template tag
    :return: the item
    """
    template = config.db.item_templates.find_one({"tag": tag})
    if template:
        return Item(template)
