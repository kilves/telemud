"""
The language file for the project

format:

translations = {
    "<language>": {
        "<tag>": "<translation>"
    }
}
"""

translations = {
    "en_US": {
        "gather": "Gather",
        "attack": "Attack",

        "wood_shards": "wood shards",
        "small_lizard_skin": "small lizard skin",
        "small_lizard_eye": "small lizard eye",

        "action_progress": "You begin doing just that...",
        "got_item": "You just got",
        "not_left": "not left",
        "look_around": "Look around",
        "enter_combat": "You enter combat with"
    },

    "fi_FI": {
        "gather": "Kerää",
        "attack": "Hyökkää",

        "wood_shards": "puunpaloja",
        "small_lizard_skin": "pienen liskon nahkaa",
        "small_lizard_eye": "pienen liskon silmiä",

        "action_progress": "Alat tekemään juuri niin...",
        "got_item": "Sait juuri",
        "not_left": "ei jäljellä.",
        "look_around": "Katso ympärillesi",
        "enter_combat": "Astut taisteluun vastaan hirviötä"
    }
}