import threading
from random import randint, uniform
import config

class Enemy(object):
    def __init__(self, enemy):
        """Initializes from enemy database object"""
        self.hp = enemy["max_hp"]
        self.max_hp = self.hp
        self.attacks = enemy["attacks"]
        self.drops = enemy["drops"]
        self.tag = enemy["tag"]
        self.combat_user = None
        self.lang = enemy["lang"]
        self.dead = False

    def drop_items(self):
        for item_drop in self.drops:
            if uniform(0.0, 100.0) < item_drop["probability"]:
                self.combat_user.give_item({
                    "item": item_drop["item"],
                    "amount": item_drop["amount"]
                })

    def deal_damage(self, damage_range):
        if not self.combat_user:
            return
        dealt_damage = randint(damage_range[0], damage_range[1])
        self.hp -= dealt_damage
        if self.hp <= 0:
            self.dead = True
            self.drop_items()
        if not self.dead:
            threading.Timer(2, self.perform_turn).start()
        else:
            self.combat_user.give_turn()
        return dealt_damage



    def perform_turn(self):
        for attack in self.attacks:
            if uniform(0.0, 100.0) < attack["probability"]:
                self.combat_user.deal_damage(attack["damage"])
                break
        self.combat_user.give_turn()


    @classmethod
    def create(cls, tag):
        enemy_object = config.db.enemies.find_one({"tag": tag})
        if enemy_object:
            return Enemy(enemy_object)